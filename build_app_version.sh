#!/usr/bin/env bash

set -e
IFS='.' read -r -a semver <<< "$(cat version.txt)"
APP_VERSION="${semver [0]}.${semver [1]}.$((${semver [2]}+1))"

./gradlew clean assemble -Pversion="${APP_VERSION}"

echo "Build APP version: ${APP_VERSION}"
echo ${APP_VERSION} > version.txt

