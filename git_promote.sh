#!/usr/bin/env bash

set -e

APP_VERSION=$(cat version.txt)
git add version.txt
git commit -m "Promoting APP version: ${APP_VERSION} to Dev."
git push